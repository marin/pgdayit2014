To compile the java program use the following command:

javac Main.java

To run the java program you need to download the latest jdbc driver for PostgreSQL from http://jdbc.postgresql.org/download.html and run the program like this:

java -cp .;postgresql-9.3-1102.jdbc4.jar Main "jdbc:postgresql://localhost/pgdayit2014" username password

Once the program is running you can use CTRL+C to stop it. If you then check pg_stat_activity you will see that the query is still running.