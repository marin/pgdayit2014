
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) {
        Connection conn = null;
        Statement statement = null;
        ResultSet rs = null;

    
        String url = args[0];
        String user = args[1];
        String password = args[2];

        try {
            conn = DriverManager.getConnection(url, user, password);
            statement=  conn.createStatement();
            rs = statement.executeQuery("select i, pg_sleep(1) from generate_series(1,10*60) i");

            while (rs.next()) {
                System.out.println(rs.getString(1));
            }

        } catch (SQLException ex) {
            System.out.println(ex);

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
                System.out.println(ex);
            }
        }
    }
}
